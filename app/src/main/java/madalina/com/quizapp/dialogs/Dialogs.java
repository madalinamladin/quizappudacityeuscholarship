package madalina.com.quizapp.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Madalina Mladin on 02/04/2017.
 */
public class Dialogs {

    /** Declaring current context variable **/
    private Context currentContext;

    /**
     * @desc default constructor
     * @param currentContext
     * @return
     */
    public Dialogs(Context currentContext){
        this.currentContext = currentContext;
    }

    /**
     * @desc show an alert dialog
     * @param message
     * @return void
     */
    public void showAlertDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(currentContext);
        builder.setTitle(message);
        String[] types = {"OK"};
        builder.setItems(types, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                switch (which) {
                    case 0:
                        dialog.cancel();
                        break;
                }
            }
        });
        builder.show();
    }
}
