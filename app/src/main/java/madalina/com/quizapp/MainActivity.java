package madalina.com.quizapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import madalina.com.quizapp.fragments.Question1Fragment;
import madalina.com.quizapp.fragments.Question2Fragment;
import madalina.com.quizapp.fragments.Question3Fragment;
import madalina.com.quizapp.fragments.Question4Fragment;
import madalina.com.quizapp.fragments.Question5Fragment;
import madalina.com.quizapp.fragments.ScoreFragment;
import madalina.com.quizapp.fragments.WelcomeFragment;

public class MainActivity extends AppCompatActivity {

    /** Declaring Welcome Fragment **/
    private WelcomeFragment mWelcomeFragment;
    /** Declaring Question1 Fragment **/
    private Question1Fragment mQuestion1Fragment;
    /** Declaring Question2 Fragment **/
    private Question2Fragment mQuestion2Fragment;
    /** Declaring Question3 Fragment **/
    private Question3Fragment mQuestion3Fragment;
    /** Declaring Question4 Fragment **/
    private Question4Fragment mQuestion4Fragment;
    /** Declaring Question5 Fragment **/
    private Question5Fragment mQuestion5Fragment;
    /** Declaring Score Fragment **/
    private ScoreFragment mScoreFragment;

    /** Declaring the variable witch will keep the quiz score **/
    public int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /** Inflating main activity xml **/
        setContentView(R.layout.activity_main);

        /** Inflating welcome fragment **/
        mWelcomeFragment = (WelcomeFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_welcome);
        /** Inflating question1 fragment **/
        mQuestion1Fragment = (Question1Fragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_question1);
        /** Inflating question2 fragment **/
        mQuestion2Fragment = (Question2Fragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_question2);
        /** Inflating question3 fragment **/
        mQuestion3Fragment = (Question3Fragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_question3);
        /** Inflating question4 fragment **/
        mQuestion4Fragment = (Question4Fragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_question4);
        /** Inflating question5 fragment **/
        mQuestion5Fragment = (Question5Fragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_question5);
        /** Inflating score fragment **/
        mScoreFragment = (ScoreFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_score);

        openWelcome();
    }

    /**
     * @desc make welcome fragment visible
     * @param
     * @return void
     */
    public void ShowWelcomeFragment(){
        mWelcomeFragment = (WelcomeFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_welcome);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.show(mWelcomeFragment);
        ft.commitAllowingStateLoss();
    }
    /**
     * @desc make welcome fragment invisible
     * @param
     * @return void
     */
    public void HideWelcomeFragment(){
        mWelcomeFragment= (WelcomeFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_welcome);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.hide(mWelcomeFragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * @desc make question1 fragment visible
     * @param
     * @return void
     */
    public void ShowQuestion1Fragment(){
        mQuestion1Fragment = (Question1Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question1);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.show(mQuestion1Fragment);
        ft.commitAllowingStateLoss();
    }
    /**
     * @desc make question1 fragment invisible
     * @param
     * @return void
     */
    public void HideQuestion1Fragment(){
        mQuestion1Fragment= (Question1Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question1);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.hide(mQuestion1Fragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * @desc make question2 fragment visible
     * @param
     * @return void
     */
    public void ShowQuestion2Fragment(){
        mQuestion2Fragment = (Question2Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question2);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.show(mQuestion2Fragment);
        ft.commitAllowingStateLoss();
    }
    /**
     * @desc make question2 fragment invisible
     * @param
     * @return void
     */
    public void HideQuestion2Fragment(){
        mQuestion2Fragment= (Question2Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question2);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.hide(mQuestion2Fragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * @desc make question3 fragment visible
     * @param
     * @return void
     */
    public void ShowQuestion3Fragment(){
        mQuestion3Fragment = (Question3Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question3);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.show(mQuestion3Fragment);
        ft.commitAllowingStateLoss();
    }
    /**
     * @desc make question3 fragment invisible
     * @param
     * @return void
     */
    public void HideQuestion3Fragment(){
        mQuestion3Fragment= (Question3Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question3);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.hide(mQuestion3Fragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * @desc make question4 fragment visible
     * @param
     * @return void
     */
    public void ShowQuestion4Fragment(){
        mQuestion4Fragment = (Question4Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question4);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.show(mQuestion4Fragment);
        ft.commitAllowingStateLoss();
    }
    /**
     * @desc make question4 fragment invisible
     * @param
     * @return void
     */
    public void HideQuestion4Fragment(){
        mQuestion4Fragment= (Question4Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question4);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.hide(mQuestion4Fragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * @desc make question5 fragment visible
     * @param
     * @return void
     */
    public void ShowQuestion5Fragment(){
        mQuestion5Fragment = (Question5Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question5);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.show(mQuestion5Fragment);
        ft.commitAllowingStateLoss();
    }
    /**
     * @desc make question5 fragment invisible
     * @param
     * @return void
     */
    public void HideQuestion5Fragment(){
        mQuestion5Fragment= (Question5Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_question5);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.hide(mQuestion5Fragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * @desc make score fragment visible
     * @param
     * @return void
     */
    public void ShowScoreFragment(){
        mScoreFragment = (ScoreFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_score);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.show(mScoreFragment);
        ft.commitAllowingStateLoss();
    }
    /**
     * @desc make score fragment invisible
     * @param
     * @return void
     */
    public void HideScoreFragment(){
        mScoreFragment= (ScoreFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_score);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.hide(mScoreFragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * @desc hide all fragments
     * @param
     * @return void
     */
    public void HideAllFragments(){
        HideWelcomeFragment();
        HideQuestion1Fragment();
        HideQuestion2Fragment();
        HideQuestion3Fragment();
        HideQuestion4Fragment();
        HideQuestion5Fragment();
        HideScoreFragment();
    }

    /**
     * @desc show welcome fragment
     * @param
     * @return void
     */
    public void openWelcome(){
        HideAllFragments();
        ShowWelcomeFragment();
    }

    /**
     * @desc show question1 fragment
     * @param
     * @return void
     */
    public void openQuestion1(){
        HideAllFragments();
        ShowQuestion1Fragment();
    }

    /**
     * @desc show question2 fragment
     * @param
     * @return void
     */
    public void openQuestion2(){
        HideAllFragments();
        ShowQuestion2Fragment();
    }

    /**
     * @desc show question3 fragment
     * @param
     * @return void
     */
    public void openQuestion3(){
        HideAllFragments();
        ShowQuestion3Fragment();
    }

    /**
     * @desc show question4 fragment
     * @param
     * @return void
     */
    public void openQuestion4(){
        HideAllFragments();
        ShowQuestion4Fragment();
    }

    /**
     * @desc show question5 fragment
     * @param
     * @return void
     */
    public void openQuestion5(){
        HideAllFragments();
        ShowQuestion5Fragment();
    }

    /**
     * @desc show score fragment
     * @param
     * @return void
     */
    public void openScore(){
        HideAllFragments();
        ShowScoreFragment();
        mScoreFragment.textViewScore.setText(Integer.toString(score) + "/10");
    }

    public void resetQuestions(){
        mQuestion1Fragment.resetQuestion1();
        mQuestion2Fragment.resetQuestion2();
        mQuestion3Fragment.resetQuestion3();
        mQuestion4Fragment.resetQuestion4();
        mQuestion5Fragment.resetQuestion5();
        mScoreFragment.resetScore();
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_blank,
                    container, false);
            return rootView;
        }
    }
}
