package madalina.com.quizapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import madalina.com.quizapp.MainActivity;
import madalina.com.quizapp.R;
import madalina.com.quizapp.dialogs.Dialogs;

/**
 * Created by Madalina Mladin on 01/04/2017.
 */
public class Question4Fragment extends Fragment {

    /** Declaring MainActivity mainActivity variable **/
    private MainActivity mainActivity;

    /** Declaring Dialogs dialogs variable **/
    private Dialogs dialogs;


    /** Declaring RadioButton radioButton1Question4 variable **/
    private RadioButton radioButton1Question4;

    /** Declaring RadioButton radioButton2Question4 variable **/
    private RadioButton radioButton2Question4;

    /** Declaring RadioButton radioButton3Question4 variable **/
    private RadioButton radioButton3Question4;

    /** Declaring Button buttonQuestion4 variable **/
    private Button buttonQuestion4;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        /** Inflating question4 fragment xml **/
        return inflater.inflate(R.layout.fragment_question4, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /** Initialization of mainActivity variable **/
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            mainActivity = (MainActivity) activity;
        }

        /** Instantiation and initialization of dialogs variable **/
        dialogs = new Dialogs(mainActivity);

        /** Inflating radioButton1Question4 RadioButton **/
        radioButton1Question4 = (RadioButton) getView().findViewById(R.id.radioButton1Question4);

        /** Inflating radioButton2Question4RadioButton **/
        radioButton2Question4 = (RadioButton) getView().findViewById(R.id.radioButton2Question4);

        /** Inflating radioButton3Question4RadioButton **/
        radioButton3Question4 = (RadioButton) getView().findViewById(R.id.radioButton3Question4);

        /** Inflating buttonQuestion4 Button **/
        buttonQuestion4 = (Button) getView().findViewById(R.id.buttonQuestion4);

        /** Set OnClickListener for buttonQuestion4 **/
        buttonQuestion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /** Retrieve radioButton1Question4 checked state **/
                boolean checked1 = radioButton1Question4.isChecked();

                /** Retrieve radioButton2Question4 checked state **/
                boolean checked2 = radioButton2Question4.isChecked();

                /** Retrieve radioButton3Question4 checked state **/
                boolean checked3 = radioButton3Question4.isChecked();

                /** Check if at least one radio button is checked **/
                if (checked1 == false && checked2 == false && checked3 == false){

                    /** Display an alert dialog witch inform user that he didn't checked any radio button **/
                    dialogs.showAlertDialog(getString(R.string.alert_dialog_please_complete_the_answer));
                    /** Stop the execution **/
                    return;
                }

                /** Check if the correct answer have been checked **/
                if ( checked3==true ){

                    /** Add 2 points to total score because user answer is correct **/
                    mainActivity.score= mainActivity.score + 2;
                }

                /** Display the question 5 **/
                mainActivity.openQuestion5();
            }
        });
    }

    /**
     * @desc reset question 4 fields
     * @param
     * @return void
     */
    public void resetQuestion4(){

        /** reset radioButton1Question4 **/
        radioButton1Question4.setChecked(false);

        /** reset radioButton2Question4 **/
        radioButton2Question4.setChecked(false);

        /** reset radioButton3Question4**/
        radioButton3Question4.setChecked(false);
    }


}

