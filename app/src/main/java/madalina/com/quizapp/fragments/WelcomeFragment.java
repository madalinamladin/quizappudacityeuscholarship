package madalina.com.quizapp.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import madalina.com.quizapp.MainActivity;
import madalina.com.quizapp.R;

/**
 * Created by Madalina Mladin on 01/04/2017.
 */
public class WelcomeFragment extends Fragment {

    /** Declaring MainActivity mainActivity variable **/
    private MainActivity mainActivity;

    /** Declaring Button buttonWelcomeStart variable **/
    private Button buttonWelcomeStart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        /** Inflating welcome fragment xml **/
        return inflater.inflate(R.layout.fragment_welcome, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /** Initialization of mainActivity variable **/
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            mainActivity = (MainActivity) activity;
        }

        /** Inflating buttonWelcomeStart Button **/
        buttonWelcomeStart = (Button) getView().findViewById(R.id.buttonWelcomeStart);

        /** Set OnClickListener for  buttonWelcomeStart **/
        buttonWelcomeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /** Display the question 1 **/
                mainActivity.openQuestion1();

            }
        });
    }


}
