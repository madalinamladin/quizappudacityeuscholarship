package madalina.com.quizapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import madalina.com.quizapp.MainActivity;
import madalina.com.quizapp.R;

/**
 * Created by Madalina Mladin on 01/04/2017.
 */
public class ScoreFragment extends Fragment {

    /** Declaring MainActivity mainActivity variable **/
    private MainActivity mainActivity;

    /** Declaring TextView textViewScore variable **/
    public TextView textViewScore;

    /** Declaring Button buttonScoreRestart variable **/
    private Button buttonScoreRestart;

    /** Declaring Button buttonScoreClose variable **/
    private Button buttonScoreClose;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        /** Inflating score fragment xml **/
        return inflater.inflate(R.layout.fragment_score, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /** Initialization of mainActivity variable **/
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            mainActivity = (MainActivity) activity;
        }

        /** Inflating textViewScore TextView **/
        textViewScore = (TextView) getView().findViewById(R.id.textViewScore);

        /** Inflating buttonScoreRestart Button **/
        buttonScoreRestart = (Button) getView().findViewById(R.id.buttonScoreRestart);

        /** Set OnClickListener for  buttonScoreRestart **/
        buttonScoreRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Restart quiz and reset all fields **/
                mainActivity.openWelcome();
                mainActivity.resetQuestions();
            }
        });

        /** Inflating buttonScoreClose Button **/
        buttonScoreClose = (Button) getView().findViewById(R.id.buttonScoreClose);

        /** Set OnClickListener for  buttonScoreClose **/
        buttonScoreClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Stop application **/
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });
    }

    /**
     * @desc reset score text view
     * @param
     * @return void
     */
    public void resetScore(){
        /** Reset score text view **/
        textViewScore.setText("0/0");
    }


}