package madalina.com.quizapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import madalina.com.quizapp.MainActivity;
import madalina.com.quizapp.R;
import madalina.com.quizapp.dialogs.Dialogs;

/**
 * Created by Madalina Mladin on 01/04/2017.
 */
public class Question3Fragment extends Fragment {

    /** Declaring MainActivity mainActivity variable **/
    private MainActivity mainActivity;

    /** Declaring Dialogs dialogs variable **/
    private Dialogs dialogs;

    /** Declaring CheckBox checkBoxButton1 variable **/
    private CheckBox checkBoxButton1;

    /** Declaring CheckBox checkBoxButton2 variable **/
    private CheckBox checkBoxButton2;

    /** Declaring CheckBox checkBoxButton3 variable **/
    private CheckBox checkBoxButton3;

    /** Declaring CheckBox checkBoxButto4 variable **/
    private CheckBox checkBoxButton4;

    /** Declaring CheckBox checkBoxButton5 variable **/
    private CheckBox checkBoxButton5;

    /** Declaring Button buttonQuestion3 variable **/
    private Button buttonQuestion3;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        /** Inflating question3 fragment xml **/
        return inflater.inflate(R.layout.fragment_question3, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /** Initialization of mainActivity variable **/
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            mainActivity = (MainActivity) activity;
        }

        /** Instantiation and initialization of dialogs variable **/
        dialogs = new Dialogs(mainActivity);

        /** Inflating checkBoxButton1 CheckBox **/
        checkBoxButton1 = (CheckBox) getView().findViewById(R.id.checkBoxButton1);

        /** Inflating checkBoxButton2 CheckBox **/
        checkBoxButton2 = (CheckBox) getView().findViewById(R.id.checkBoxButton2);

        /** Inflating checkBoxButton3 CheckBox **/
        checkBoxButton3 = (CheckBox) getView().findViewById(R.id.checkBoxButton3);

        /** Inflating checkBoxButton4 CheckBox **/
        checkBoxButton4 = (CheckBox) getView().findViewById(R.id.checkBoxButton4);

        /** Inflating checkBoxButton5 CheckBox **/
        checkBoxButton5 = (CheckBox) getView().findViewById(R.id.checkBoxButton5);

        /** Inflating buttonQuestion3 Button **/
        buttonQuestion3 = (Button) getView().findViewById(R.id.buttonQuestion3);

        /** Set OnClickListener for buttonQuestion3Button **/
        buttonQuestion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /** Retrieve checkBoxButton1 checked state **/
                boolean checked1 = checkBoxButton1.isChecked();

                /** Retrieve checkBoxButton2 checked state **/
                boolean checked2 = checkBoxButton2.isChecked();

                /** Retrieve checkBoxButton3 checked state **/
                boolean checked3 = checkBoxButton3.isChecked();

                /** Retrieve checkBoxButton4 checked state **/
                boolean checked4 = checkBoxButton4.isChecked();

                /** Retrieve checkBoxButton5 checked state **/
                boolean checked5 = checkBoxButton5.isChecked();

                /** Check if at least one checkbox is checked **/
                if (checked1 == false && checked2 == false && checked3 == false && checked4 == false && checked5 == false ){

                    /** Display an alert dialog witch inform user that he didn't checked any checkbox **/
                    dialogs.showAlertDialog(getString(R.string.alert_dialog_please_complete_the_answer));
                    /** Stop the execution **/
                    return;
                }

                /** Check if the correct answer have been checked **/
                if ( checked1==true && checked3==true && checked4==true ){

                    /** Add 2 points to total score because user answer is correct **/
                    mainActivity.score= mainActivity.score + 2;

                }

                /** Display the question 4 **/
                mainActivity.openQuestion4();
            }
        });
    }

    /**
     * @desc reset question 3 fields
     * @param
     * @return void
     */
    public void resetQuestion3(){

        /** reset checkBoxButton1 **/
        checkBoxButton1.setChecked(false);

        /** reset checkBoxButton2 **/
        checkBoxButton2.setChecked(false);

        /** reset checkBoxButton3 **/
        checkBoxButton3.setChecked(false);

        /** reset checkBoxButton4 **/
        checkBoxButton4.setChecked(false);

        /** reset checkBoxButton5 **/
        checkBoxButton5.setChecked(false);
    }


}
