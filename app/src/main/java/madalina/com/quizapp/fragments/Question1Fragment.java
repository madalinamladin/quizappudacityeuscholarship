package madalina.com.quizapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import madalina.com.quizapp.MainActivity;
import madalina.com.quizapp.R;
import madalina.com.quizapp.dialogs.Dialogs;

/**
 * Created by Madalina Mladin on 01/04/2017.
 */
public class Question1Fragment extends Fragment {

    /** Declaring MainActivity mainActivity variable **/
    private MainActivity mainActivity;

    /** Declaring Dialogs dialogs variable **/
    private Dialogs dialogs;

    /** Declaring EditText editTextQuestion1 variable **/
    private EditText editTextQuestion1;

    /** Declaring Button buttonQuestion1 variable **/
    private Button buttonQuestion1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        /** Inflating question1 fragment xml **/
        return inflater.inflate(R.layout.fragment_question1, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /** Initialization of mainActivity variable **/
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            mainActivity = (MainActivity) activity;
        }

        /** Instantiation and initialization of dialogs variable **/
        dialogs = new Dialogs(mainActivity);

        /** Inflating editTextQuestion1 EditText **/
        editTextQuestion1 = (EditText) getView().findViewById(R.id.editTextQuestion1);

        /** Inflating buttonQuestion1 Button **/
        buttonQuestion1 = (Button) getView().findViewById(R.id.buttonQuestion1);

        /** Set OnClickListener for buttonQuestion1 Button **/
        buttonQuestion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /** Defining the correct answer for question 1 **/
                int correctAnswer = 8848;

                /** Get the user answer for question 1 in String format **/
                String stringUserAnswer = editTextQuestion1.getText().toString();

                /** Check the user answer to see if it's empty **/
                if (stringUserAnswer.equals("")) {

                    /** Display an alert dialog witch inform user that he didn't completed an answer for question 1 **/
                    dialogs.showAlertDialog(getString(R.string.alert_dialog_please_complete_the_answer));
                    /** Stop the execution **/
                    return;
                }

                /** Convert user answer from String to int in order to compare it **/
                int userAnswer = Integer.parseInt(stringUserAnswer);

                /** Compare the user answer with the correct answer **/
                if ( correctAnswer==userAnswer){

                    /** Add 2 points to total score because user answer is correct **/
                    mainActivity.score= mainActivity.score + 2;

                }

                /** Display the question 2 **/
                mainActivity.openQuestion2();
            }
        });
    }

    /**
     * @desc reset question 1 fields
     * @param
     * @return void
     */
    public void resetQuestion1(){

        /** reset editTextQuestion1 **/
        editTextQuestion1.setText("");

    }


}
