package madalina.com.quizapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import madalina.com.quizapp.MainActivity;
import madalina.com.quizapp.R;
import madalina.com.quizapp.dialogs.Dialogs;

/**
 * Created by Madalina Mladin on 01/04/2017.
 */
public class Question2Fragment extends Fragment {

    /** Declaring MainActivity mainActivity variable **/
    private MainActivity mainActivity;

    /** Declaring Dialogs dialogs variable **/
    private Dialogs dialogs;


    /** Declaring RadioButton radioButton1Question2 variable **/
    private RadioButton radioButton1Question2;

    /** Declaring RadioButton radioButton2Question2 variable **/
    private RadioButton radioButton2Question2;

    /** Declaring RadioButton radioButton3Question2 variable **/
    private RadioButton radioButton3Question2;


    /** Declaring Button buttonQuestion2 variable **/
    private Button buttonQuestion2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        /** Inflating question2 fragment xml **/
        return inflater.inflate(R.layout.fragment_question2, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /** Initialization of mainActivity variable **/
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            mainActivity = (MainActivity) activity;
        }

        /** Instantiation and initialization of dialogs variable **/
        dialogs = new Dialogs(mainActivity);


        /** Inflating radioButton1Question2 RadioButton **/
        radioButton1Question2 = (RadioButton) getView().findViewById(R.id.radioButton1Question2);

        /** Inflating radioButton2Question2 RadioButton **/
        radioButton2Question2 = (RadioButton) getView().findViewById(R.id.radioButton2Question2);

        /** Inflating radioButton3Question2 RadioButton **/
        radioButton3Question2 = (RadioButton) getView().findViewById(R.id.radioButton3Question2);


        /** Inflating buttonQuestion2 Button **/
        buttonQuestion2 = (Button) getView().findViewById(R.id.buttonQuestion2);

        /** Set OnClickListener for buttonQuestion2 Button **/
        buttonQuestion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /** Retrieve radioButton1Question2 checked state **/
                boolean checked1 = radioButton1Question2.isChecked();

                /** Retrieve radioButton2Question2 checked state **/
                boolean checked2 = radioButton2Question2.isChecked();

                /** Retrieve radioButton3Question2 checked state **/
                boolean checked3 = radioButton3Question2.isChecked();

                /** Check if at least one radio button is checked **/
                if (checked1 == false && checked2 == false && checked3 == false){

                    /** Display an alert dialog witch inform user that he didn't checked any radio button **/
                    dialogs.showAlertDialog(getString(R.string.alert_dialog_please_complete_the_answer));
                    /** Stop the execution **/
                    return;

                }

                /** Check if the correct answer have been checked **/
                if ( checked2==true ){

                    /** Add 2 points to total score because user answer is correct **/
                    mainActivity.score= mainActivity.score + 2;
                }

                /** Display the question 3 **/
                mainActivity.openQuestion3();

            }
        });
    }

    /**
     * @desc reset question 2 fields
     * @param
     * @return void
     */
    public void resetQuestion2(){

        /** reset radioButton1Question2 **/
        radioButton1Question2.setChecked(false);

        /** reset radioButton2Question2 **/
        radioButton2Question2.setChecked(false);

        /** reset radioButton3Question2 **/
        radioButton3Question2.setChecked(false);
    }
}

